from collections import deque
from django.shortcuts import render
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from todos.models import TodoItem, TodoList
from django.urls import include, path, reverse_lazy

# Create your views here.

class TodoListView(ListView):
    model = TodoList
    template_name = "todo_lists/list.html"


class TodoListDetail(DetailView):
    model = TodoList
    template_name = "todo_lists/detail.html"
    

class TodoListCreate(CreateView):
    model = TodoList
    template_name = "todo_lists/add.html"
    fields = ["name"]
    success_url = reverse_lazy("todo_list")

    def form_valid(self, form):
        return super().form_valid(form)
    

class TodoListUpdate(UpdateView):
    model = TodoList
    template_name = "todo_lists/edit.html"
    name = TodoList.name
    fields = ["name"]
    success_url = reverse_lazy("todo_list")
    

class TodoListDelete(DeleteView):

    model = TodoList
    template_name = "todo_lists/delete.html"
    

class TodoItemCreate(CreateView):
    model = TodoItem
    template_name = "todo_items/create.html"
    fields = ["task", "due_date", "is_completed", "list"]
    success_url = reverse_lazy("todo_list")

    def form_valid(self, form):
        return super().form_valid(form)

class TodoItemUpdate(UpdateView):
    model = TodoItem
    template_name = "todo_items/update.html"
    task = TodoItem.task
    due_date = TodoItem.due_date
    is_completed = TodoItem.is_completed
    list = TodoItem.list
    fields = ["task", "due_date", "is_completed", "list"]
    success_url = reverse_lazy("todo_list")