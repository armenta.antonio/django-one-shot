from django.urls import path
from todos.views import (
    TodoListCreate, 
    TodoListDelete, 
    TodoListUpdate, 
    TodoListView, 
    TodoListDetail, 
    TodoItemCreate, 
    TodoItemUpdate
)

urlpatterns = [
    path("", TodoListView.as_view(), name="todo_list"), 
    path("<int:pk>/", TodoListDetail.as_view(), name="todo_list_detail"),
    path("<int:pk>/edit/", TodoListUpdate.as_view(), name="todo_list_update"),
    path("add/", TodoListCreate.as_view(), name="todo_list_add"),
    path("<int:pk>/delete/", TodoListDelete.as_view(), name="todo_list_delete"),
    path("items/new/", TodoItemCreate.as_view(), name="todo_item_create"),
    path("items/<int:pk>/edit/", TodoItemUpdate.as_view(), name="todo_item_update"),
]